package com.example.looping;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText inputPerkalian;
    Button btnHitung;
    TextView txtHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        Objects.requireNonNull(getSupportActionBar()).setTitle(getString(R.string.action_bar_title));
    }

    private void initComponents() {
        inputPerkalian = findViewById(R.id.txtInputPerkalian);
        btnHitung = findViewById(R.id.btnBuatPerkalian);
        txtHasil = findViewById(R.id.txtTampilPerkalian);

        btnHitung.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnBuatPerkalian) {
            int input = (inputPerkalian.getText().toString().isEmpty()) ? 0 : Integer.parseInt(inputPerkalian.getText().toString());

            String output = "";

            for(int i = 1; i <= 10; i++) {

                output += input + " pangkat " + i + " adalah ";
                int temp = 1;

                for(int j = 1; j <= i; j++) {
                    temp *= input;
                }

                output += temp + "\n";
            }

            txtHasil.setText(output);

        }
    }
}
